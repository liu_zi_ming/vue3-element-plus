


# Vue 3 - Element-plus 开发模板


## 项目介绍
该项目是基于 Vue 3 + TypeScript + Vite + pinia + axios +  element-plus 开发模板。

## 项目特点
1. ✨Vue 3
2. ✨TypeScript
3. ✨Vite
4. ✨pinia
5. ✨axios
6. ✨element-plus

  该模板应该可以帮助您开始在 Vite 中使用 Vue 3 和 TypeScript 进行开发。该模板使用 Vue 3 `<script setup>` SFC，请查看[脚本设置文档](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) 来学习更多的。

